from string import printable

KEY = b"SUP3RS3CR3TK3Y"
CHECK = bytearray(
    b"\xe9\xef\xc0V\x8d\x8a\x05\xbe\x8ek\xd9yX\x8b\x89\xd3\x8c\xfa\xdexu\xbe\xdf1\xde\xb6\\"
)


def transform(character, index):
    return (character + 24 & 255 ^ KEY[index % len(KEY)]) - 74 & 255


if __name__ == "__main__":
    flag = ""

    for i in range(len(CHECK)):
        for j in printable:
            transformed = transform(ord(j), i)
            if transformed == CHECK[i]:
                flag += j
                break

    print(flag)
