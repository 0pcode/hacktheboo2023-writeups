# SpellBrewery

```console
opcode@parrot$ unzip -l rev_spellbrewery.zip        
Archive:  rev_spellbrewery.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
        0  2023-09-21 20:39   rev_spellbrewery/
      139  2023-09-21 20:37   rev_spellbrewery/SpellBrewery.runtimeconfig.json
      406  2023-09-21 20:37   rev_spellbrewery/SpellBrewery.deps.json
   181016  2023-09-21 20:37   rev_spellbrewery/SpellBrewery
    13824  2023-09-21 20:37   rev_spellbrewery/SpellBrewery.dll
---------                     -------
   195385                     5 files
```

It seems to be a .NET executable along with a `dll`

```console
opcode@parrot$ file SpellBrewery
SpellBrewery: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=8b106946c31c6346abd618ab5d2232845492e2d9, not stripped
opcode@parrot$ file SpellBrewery.dll 
SpellBrewery.dll: PE32 executable (console) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
```

Instead of a PE32 executable, it is an ELF binary. I tried running it:

```console
opcode@parrot$ ./SpellBrewery
You must install .NET to run this application.

App: /mnt/hgfs/Opcode/HTB/HackTheBoo2023/rev_spellbrewery/SpellBrewery
Architecture: x64
App host version: 7.0.11
.NET location: Not found

Learn about runtime installation:
https://aka.ms/dotnet/app-launch-failed

Download the .NET runtime:
https://aka.ms/dotnet-core-applaunch?missing_runtime=true&arch=x64&rid=debian.12-x64&apphost_version=7.0.11
```

I'm on a fresh VM for the CTF, and `dotnet` is not installed.  
Before installing, I decided to open it on my Windows VM with [dotPeek](https://www.jetbrains.com/decompiler/) and check out the source.

If we look inside the `Menu` class in `SpellBrewery` namespace, we'd get a basic grasp of this binary:

```cs
    public static Menu.Choice RunMenu()
    {
      Console.WriteLine("1. List Ingredients");
      Console.WriteLine("2. Display Current Recipe");
      Console.WriteLine("3. Add Ingredient");
      Console.WriteLine("4. Brew Spell");
      Console.WriteLine("5. Clear Recipe");
      Console.WriteLine("6. Quit");
      Console.Write("> ");
      [--SNIP--]
    }
```

The key to the flag lies in the class `Brewery`:

```cs
namespace SpellBrewery
{
  internal class Brewery
  {
    private static readonly string[] IngredientNames = new string[106]
    {
      "Witch's Eye",
      "Bat Wing",
      "Ghostly Essence",
      [--SNIP--]
      "Cursed Charm"
    };
    private static readonly string[] correct = new string[36]
    {
      "Phantom Firefly Wing",
      "Ghastly Gourd",
      "Hocus Pocus Powder",
      [--SNIP--]
      "Wraith Whisper"
    };
    private static readonly List<Ingredient> recipe = new List<Ingredient>();

    private static void Main()
    {
      while (true)
      {
        switch (Menu.RunMenu())
        {
          case Menu.Choice.ListIngredients:
            Brewery.ListIngredients();
            break;
          case Menu.Choice.DisplayRecipe:
            Brewery.DisplayRecipe();
            break;
          case Menu.Choice.AddIngredient:
            Brewery.AddIngredient();
            break;
          case Menu.Choice.BrewSpell:
            Brewery.BrewSpell();
            break;
          case Menu.Choice.ClearRecipe:
            Brewery.ClearRecipe();
            break;
        }
      }
    }

    private static void ListIngredients()
    {
      for (int index = 0; index < Brewery.IngredientNames.Length; ++index)
      {
        Console.Write(Brewery.IngredientNames[index] ?? "");
        if (index + 1 < Brewery.IngredientNames.Length)
          Console.Write(", ");
        if (index % 6 == 5)
          Console.Write("\n");
      }
      Console.Write("\n");
    }

    private static void DisplayRecipe()
    {
      if (Brewery.recipe.Count == 0)
        Console.WriteLine("There are no current ingredients");
      else
        Console.WriteLine(string.Join<Ingredient>(", ", (IEnumerable<Ingredient>) Brewery.recipe));
    }

    private static void AddIngredient()
    {
      Console.Write("What ingredient would you like to add? ");
      string name;
      while (true)
      {
        name = Console.ReadLine();
        if (!((IEnumerable<string>) Brewery.IngredientNames).Contains<string>(name))
          Console.WriteLine("Invalid ingredient name");
        else
          break;
      }
      Brewery.recipe.Add(new Ingredient(name));
      string str = "aeiou".Contains(char.ToLower(name[0])) ? "an" : "a";
      DefaultInterpolatedStringHandler interpolatedStringHandler = new DefaultInterpolatedStringHandler(41, 2);
      interpolatedStringHandler.AppendLiteral("The cauldron fizzes as you toss in ");
      interpolatedStringHandler.AppendFormatted(str);
      interpolatedStringHandler.AppendLiteral(" '");
      interpolatedStringHandler.AppendFormatted(name);
      interpolatedStringHandler.AppendLiteral("'...");
      Console.WriteLine(interpolatedStringHandler.ToStringAndClear());
    }

    private static void BrewSpell()
    {
      if (Brewery.recipe.Count < 1)
      {
        Console.WriteLine("You can't brew with an empty cauldron");
      }
      else
      {
        byte[] array = Brewery.recipe.Select<Ingredient, byte>((Func<Ingredient, byte>) (ing => (byte) (Array.IndexOf<string>(Brewery.IngredientNames, ing.ToString()) + 32))).ToArray<byte>();
        if (Brewery.recipe.SequenceEqual<Ingredient>(((IEnumerable<string>) Brewery.correct).Select<string, Ingredient>((Func<string, Ingredient>) (name => new Ingredient(name)))))
        {
          Console.WriteLine("The spell is complete - your flag is: " + Encoding.ASCII.GetString(array));
          Environment.Exit(0);
        }
        else
          Console.WriteLine("The cauldron bubbles as your ingredients melt away. Try another recipe.");
      }
    }

    private static void ClearRecipe()
    {
      Brewery.recipe.Clear();
      Console.WriteLine("You pour the cauldron down the drain. A fizzing noise and foul smell rises from it...");
    }
  }
}
```

Essentially, we can add ingredients from a long list, and brew the concoction.  
If we use certain ingredients in a particular order, we'd get the flag:

```cs
        byte[] array = Brewery.recipe.Select<Ingredient, byte>((Func<Ingredient, byte>) (ing => (byte) (Array.IndexOf<string>(Brewery.IngredientNames, ing.ToString()) + 32))).ToArray<byte>();
        if (Brewery.recipe.SequenceEqual<Ingredient>(((IEnumerable<string>) Brewery.correct).Select<string, Ingredient>((Func<string, Ingredient>) (name => new Ingredient(name)))))
        {
          Console.WriteLine("The spell is complete - your flag is: " + Encoding.ASCII.GetString(array));
          Environment.Exit(0);
        }
```

Note that only the indices of ingredients in the list are being used to derive the flag.  
The unicode representation of `index + 32` is being used.  
It is fairly straightforward to recreate it in `Python` and get the flag:

```py
ingredients = [
    "Witch's Eye",
    "Bat Wing",
    "Ghostly Essence",
    [--SNIP--]
    "Cursed Charm",
]

correct = [
    "Phantom Firefly Wing",
    "Ghastly Gourd",
    "Hocus Pocus Powder",
    [--SNIP--]
    "Wraith Whisper",
]

for i in correct:
    print(chr(ingredients.index(i) + 32), end='')
```

```console
opcode@parrot$ python3 solve.py
HTB{y0ur3_4_tru3_p0t10n_m45st3r_n0w}
```
