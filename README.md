# Hack The Boo CTF 2022 write-ups

## Crypto

| Name | Category | Tags |
| ---  | ---      | ---  |
| [Symbols](crypto_symbols/) | Crypto | `Legendre symbol` |
| [Leaking Park](crypto_leaking_park/) | Crypto | `Operations on finite set` |
| [Pinata](pwn_pinata/) | Pwn | `execve syscall in a statically linked binary` |
| [Claw Machine](pwn_claw_machine/) | Pwn | `ret2win with PIE and canary` |
| [SpellBrewery](rev_spellbrewery/) | Reversing | `.NET reversing` |
| [SpookyCheck](rev_spookycheck/) | Reversing | `Python 3.11 bytecode reversing` |
